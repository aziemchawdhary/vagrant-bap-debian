#!/bin/bash

sudo apt-get install -y ocaml ocaml-native-compilers ocaml-findlib camlidl binutils-dev automake libcamomile-ocaml-dev otags libpcre3-dev camlp4-extra bison flex zlib1g-dev libgmp3-dev g++ libtool git
sudo apt-get install -y gcc-multilib g++-multilib lib32z1-dev

if [ ! -d "/home/vagrant/bap" ]; then
    git clone https://aziemchawdhary@bitbucket.org/aziemchawdhary/bap.git /home/vagrant/bap
    cd bap
    ./build.sh
fi
